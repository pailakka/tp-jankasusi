/* global gapi */
'use strict'
import '@babel/polyfill'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Toasted from 'vue-toasted'

import App from './components/App.vue'
import Index from './components/Index.vue'
import NewEventForm from './components/NewEventForm.vue'
import GroupDetails from './components/GroupDetails.vue'
import ScoutDetails from './components/ScoutDetails.vue'
import store from './store'

Vue.config.devtools = true
Vue.use(VueRouter)
Vue.use(Toasted)

const targetElement = document.createElement('div')
targetElement.innerHTML = 'Ladataan jänkäsusilaskuria...'

const routes = [
  { path: '/', component: Index },
  { path: '/uusi-suoritus', component: NewEventForm },
  { name: 'vartio', path: '/vartio/:vartio', component: GroupDetails },
  { name: 'scout', path: '/vartio/:vartio/:scout', component: ScoutDetails }
]
const router = new VueRouter({
  routes
})

const createApp = () => new Vue({
  el: targetElement,
  router,
  store,
  render: h => h(App)
})

var sheetsAPiElem = document.createElement('script')
sheetsAPiElem.async = true
sheetsAPiElem.defer = true
sheetsAPiElem.src = 'https://apis.google.com/js/api.js'
sheetsAPiElem.addEventListener('load', () => {
  gapi.load('client', () => {
    gapi.client.init({
      apiKey: 'AIzaSyCBVkGAS5ZuTg_-avk3yOvg2qYprig1S1k',
      discoveryDocs: ['https://sheets.googleapis.com/$discovery/rest?version=v4']

    }).then((a, b, c) => {
      createApp()
    })
  })
})
sheetsAPiElem.onreadystatechange = () => {
  console.log('sheets api', 'onreadystatechange', this.readyState)
}

document.addEventListener('DOMContentLoaded', () => {
  const jankasusi = document.getElementById('jankasusi')
  jankasusi.parentNode.appendChild(sheetsAPiElem)
  jankasusi.parentNode.appendChild(targetElement)

  //
})
