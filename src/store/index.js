import Vue from 'vue'
import Vuex from 'vuex'
import groups from './modules/groups'
import settings from './modules/settings'
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    groups,
    settings
  },
  strict: debug
})
