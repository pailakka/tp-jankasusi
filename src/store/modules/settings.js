/* global gapi */

const state = {
  groups: [],
  eventtypes: []
}

// getters
const getters = {}

// actions
const actions = {
  getSettings ({
    commit
  }) {
    gapi.client.sheets.spreadsheets.values.get({
      spreadsheetId: '1ilCSAOPZGRKMby4pta37dSRTFmTtVuOyARGZNV4gRKU',
      range: 'Asetukset!A2:B1000'
    }).then((response) => {
      commit('setGroups', response.result.values.filter(g => g[0]).map((gropuName, i) => gropuName[0]))
      commit('setEventTypes', response.result.values.filter(g => g[1]).map((eventName, i) => eventName[1]))
    })
  }
}

// mutations
const mutations = {
  setGroups (state, groups) {
    console.log('setGroups', groups)
    state.groups = groups
  },
  setEventTypes (state, eventtypes) {
    console.log('setEventTypes', eventtypes)
    state.eventtypes = eventtypes
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
