/* global gapi */
import groupBy from 'lodash/groupBy'
import sortBy from 'lodash/sortBy'

const PTkeys = ['PT-kisa', 'PT-kisa piirikunnallinen', 'PT-kisa kansallinen']

const getJankasusiStatus = (cumulative, event) => {
  if (event['3lk js'] && !cumulative.jslk3) cumulative.jslk3 = true
  if (event['2lk js'] && cumulative.jslk3 && !cumulative.jslk2) cumulative.jslk2 = true
  if (event['1lk js'] && cumulative.jslk3 && cumulative.jslk2 && !cumulative.jslk1) cumulative.jslk1 = true
  if (event['e js'] && cumulative.jslk3 && cumulative.jslk2 && cumulative.jslk1 && !cumulative.jse) cumulative.jse = true
  const byevents = cumulative.eventTypeCounts

  if (!cumulative.jslk3) {
    if (byevents['Retki'] && byevents['Retki'] >= 5) {
      if (byevents['Leiri'] && byevents['Leiri'] >= 1) {
        if (PTkeys.reduce((numPT, PTKey) => numPT + (byevents[PTKey] ? byevents[PTKey] : 0), 0) >= 1) {
          if (byevents['III luokka riihitys']) {
            cumulative.jslk3 = true
          }
        }
      }
    }
  } else {
    if (!cumulative.jslk2) {
      if (byevents['Retki'] && byevents['Retki'] >= 10) {
        if (byevents['Leiri'] && byevents['Leiri'] >= 2) {
          if (PTkeys.reduce((numPT, PTKey) => numPT + (byevents[PTKey] ? byevents[PTKey] : 0), 0) >= 2) {
            if (byevents['II luokka riihitys']) {
              cumulative.jslk2 = true
            }
          }
        }
      }
    } else {
      if (!cumulative.jslk1) {
        if (byevents['Retki'] && byevents['Retki'] >= 15) {
          if (byevents['Leiri'] && byevents['Leiri'] >= 5) {
            if (PTkeys.reduce((numPT, PTKey) => numPT + (byevents[PTKey] ? byevents[PTKey] : 0), 0) >= 5) {
              if (byevents['PT-kisa piirikunnallinen'] || byevents['PT-kisa valtakunnallinen']) {
                if (byevents['I luokka riihitys']) {
                  cumulative.jslk1 = true
                }
              }
            }
          }
        }
      } else {
        if (!cumulative.jse) {
          if (byevents['Retki'] && byevents['Retki'] >= 25) {
            if (byevents['Leiri'] && byevents['Leiri'] >= 10) {
              if (PTkeys.reduce((numPT, PTKey) => numPT + (byevents[PTKey] ? byevents[PTKey] : 0), 0) >= 10) {
                if (byevents['PT-kisa valtakunnallinen']) {
                  if (byevents['Vaellus']) {
                    cumulative.jse = true
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  const jsclasses = [
    { k: 'jse', n: 'Erikoisjänkäsusi' },
    { k: 'jslk1', n: 'I luokan jänkäsusi' },
    { k: 'jslk2', n: 'II luokan jänkäsusi' },
    { k: 'jslk3', n: 'III luokan jänkäsusi' }
  ]

  cumulative.js = ''
  for (const i in jsclasses) {
    if (cumulative[jsclasses[i].k]) {
      cumulative.js = jsclasses[i].n
      break
    }
  }

  return cumulative
}
const state = {
  events: [],
  nameSuggestions: [],
  eventsByName: {},
  scoutsByGroup: {},
  groupStats: []
}

// getters
const getters = {}

// actions
const actions = {
  getEventData ({
    commit
  }) {
    gapi.client.sheets.spreadsheets.values.get({
      spreadsheetId: '1ilCSAOPZGRKMby4pta37dSRTFmTtVuOyARGZNV4gRKU',
      range: 'Suoritukset!A1:K10000'
    }).then((response) => {
      console.log('getEventData', response.result)
      let header = null

      const names = new Set()
      const events = []
      response.result.values.map(eventRow => {
        if (header === null) {
          header = eventRow
          return true
        }

        // Make objects from eventrows based on headers
        const event = header.reduce((a, v, i) => {
          return {
            [v]: eventRow[i],
            ...a
          }
        }, {})

        events.push(event)
        names.add(event['nimi'])
      })

      commit('setEvents', events)
      commit('setNameSuggestions', [...names])
    })
  }

}

// mutations
const mutations = {
  setEvents (state, events) {
    state.events = events
    const eventsByName = groupBy(events, 'nimi')
    const scoutsByGroup = {}
    const groupStats = []

    console.log('eventsByName', 'berfore', eventsByName)
    for (const k in eventsByName) {
      const cumulativeBase = {
        lastGroup: null,
        outdoorNights: 0,
        numEvents: 0,
        eventTypeCounts: {},
        jslk3: false,
        jslk2: false,
        jslk1: false,
        jse: false
      }

      eventsByName[k].map(event => {
        if (event['3lk js']) cumulativeBase['3lkfixed'] = true
        if (event['2lk js']) cumulativeBase['2lkfixed'] = true
        if (event['1lk js']) cumulativeBase['1lkfixed'] = true
        if (event['e js']) cumulativeBase['efixed'] = true
      })
      eventsByName[k] = eventsByName[k].map(event => {
        event.cumulative = cumulativeBase
        event.Timestamp = Date.parse(event.Timestamp)
        event.cumulative.lastGroup = event.vartio

        return event
      })

      eventsByName[k] = sortBy(eventsByName[k], ['pvm', 'Timestamp'])

      eventsByName[k].reduce((accEvent, event, i) => {
        const cumulative = { ...accEvent.cumulative }

        cumulative.outdoorNights += parseInt(event.maastoyot)
        cumulative.numEvents++
        cumulative.eventTypeCounts = { ...cumulative.eventTypeCounts, ...{ [event.tapahtumatyyppi]: 1 + (cumulative.eventTypeCounts[event.tapahtumatyyppi] ? cumulative.eventTypeCounts[event.tapahtumatyyppi] : 0) } }
        cumulative.i = i
        eventsByName[k][i].cumulative = { ...cumulative, ...getJankasusiStatus(cumulative, eventsByName[k][i]) }

        return { ...eventsByName[k][i] }
      }, { ...eventsByName[k][0] })

      const lastEvent = eventsByName[k][eventsByName[k].length - 1]
      if (scoutsByGroup[lastEvent.cumulative.lastGroup] === undefined) {
        scoutsByGroup[lastEvent.cumulative.lastGroup] = []
      }
      scoutsByGroup[lastEvent.cumulative.lastGroup].push({ nimi: lastEvent.nimi, vartio: lastEvent.vartio, ...lastEvent.cumulative })
    }

    console.log('eventsByName', 'after', eventsByName)

    state.eventsByName = eventsByName
    state.scoutsByGroup = scoutsByGroup

    for (const gk in scoutsByGroup) {
      groupStats.push({ group: gk,
        ...scoutsByGroup[gk].reduce((stats, scout) => {
          stats.outdoorNights += scout.outdoorNights
          stats.numEvents += scout.numEvents
          if (scout.jse) stats.jse++
          if (scout.jslk1) stats.jslk1++
          if (scout.jslk2) stats.jslk2++
          if (scout.jslk3) stats.jslk3++

          for (const k in scout.eventTypeCounts) {
            if (stats[k] === undefined) stats[k] = 0
            stats[k] += scout.eventTypeCounts[k]
          }
          return stats
        }, {
          jse: 0,
          jslk1: 0,
          jslk2: 0,
          jslk3: 0,
          outdoorNights: 0,
          numEvents: 0
        }) })

      state.scoutsByGroup[gk] = sortBy(state.scoutsByGroup[gk], ['outdoorNights', 'jslk3', 'jslk2', 'jslk1', 'jse'])
      state.scoutsByGroup[gk].reverse()
    }

    const groupStatsSorted = sortBy(groupStats, ['outdoorNights', 'jslk3', 'jslk2', 'jslk1', 'jse', 'Retki'])
    groupStatsSorted.reverse()

    state.groupStats = groupStatsSorted

    console.log('scoutsByGroup', state.scoutsByGroup)
    console.log('groupStats', state.groupStats)
  },

  setNameSuggestions (state, names) {
    state.nameSuggestions = names
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
